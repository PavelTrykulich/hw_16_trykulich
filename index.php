<?php
include_once 'Array/arrayString.php';
include_once 'Array/arrayWords.php';


?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Regular</title>
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <style>
        .text {
            text-align:  center;
        }
    </style>
</head>
<body>
<!--Header block start-->
<div>
    <? include "header.html";?>
</div>
<!--Header block end-->

<!--Content block start-->
<br>
<div class="text">
<?php
    foreach ($search_strings as $key=>$string):
        echo '<h4>' . 'In the sentence №' . ($key+1) . '</h4>' . $string . '<br>' ;
        echo  '<h5>Contains the word:</h5>';
        foreach ($search_words as $word):
            if(preg_match("/.*($word)+.*/ui",$string)):
            echo '<h3>' . $word . '</h3>';
            echo '<br>';
            endif;
        endforeach;
    endforeach;

?>
</div>

<!--Content block end-->

<!--Footer block start-->
<div>
    <? include "footer.html";?>
</div>
<!--Footer block end-->



</body>
</html>
